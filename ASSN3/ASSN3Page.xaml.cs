﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
    public class WebURL
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int id { get; set; }

        public string url { get; set; }
        public string image { get; set; }
        public string title { get; set; }
    }

    public partial class ASSN3Page : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private List<WebURL> urls;

        public ASSN3Page()
        {
            InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }

        async void ReloadPicker()
        {
            await _connection.CreateTableAsync<WebURL>();
            urls = await _connection.Table<WebURL>().ToListAsync();

            if (ShowURLs.Items.Count != 0)
            {
                ShowURLs.Items.Clear();
            }

            foreach (var x in urls)
            {
                ShowURLs.Items.Add($"{x.title}");
            }
        }

        protected override void OnAppearing()
        {
            ReloadPicker();

            base.OnAppearing();
        }

        async void OpenWebView(object sender, System.EventArgs e)
        {
          
        }

        async void AddURLAction(object sender, System.EventArgs e)
        {
            
        }

        async void UpdateURLAction(object sender, System.EventArgs e)
        {
            
        }

        async void REMOVEURL(object sender, System.EventArgs e)
        {
            

        }
    }
}

