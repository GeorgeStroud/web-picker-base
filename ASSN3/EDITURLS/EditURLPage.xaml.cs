﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
    public partial class EditURLPage : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<WebURL> _url;
        private int SelectedNumber;

        public EditURLPage(WebURL selectedClass, int selection)
        {
            InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();

            BindingContext = selectedClass;
            SelectedNumber = selection;
        }

        async void EditURL(object sender, EventArgs e)
        {

        }

        async void GoBack(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

    }
}
